/*
Создайте класс Student который принимает в качестве аргумента в конструкторе объект enrollee (абитурент). У экземпляра класса Student должны быть поля:
id - уникальный идентификатор студента (генерируется при создании экземпляра и начинается с 1);
name - имя студента (передаем в объекте enrollee);
surname - фамилия студента (передаем в объекте enrollee);
ratingPoint - рейтинг студента по результатам вступительных экзаменов (передаем в объекте enrollee);
schoolPoint - рейтинг студента по результатам ЗНО (передаем в объекте enrollee);
isSelfPayment - если true, то студент на контракте, если false - на бюджете (генерируется по логике указанной ниже).
tips: для генерации id, можно создать отдельное статическое свойство в классе (static id = 1) и обращаться потом к нему в любом месте класса (Student.id)
*/

class Student {
  static idStatic = 1;

  constructor({name, surname, ratingPoint, schoolPoint}) {
    this.id = Student.idStatic++;
    this.name = name;
    this.surname = surname;
    this.ratingPoint = ratingPoint;
    this.schoolPoint = schoolPoint;
    this.isSelfPayment = true;
  }
}

/*
Создайте класс University со свойством name и методом addStudent().
Метод addStudend() должен реализовывать следующую логику:
Добавлять студентов в массив студентов университета;
Если у студента ratingPoint больше или равен 800, то студент может быть зачислен на бюджет, при условии что он будет входить в пятерку (5) лучших студентов рейтинга (напомню что рейтинг считается как сумма результатов ЗНО и вступительных экзаменов - schoolPoint и ratingPoint);
Устанавливать правильное значение isSelfPayment для каждого студента;
Добавьте так же методы для получения данных о списках зачисленных студентов и студентов, которые зачислены на бюджет.
*/

let universityStudents = [];

class University {
  static maxBudgetStudents = 5;
  static minBudgetScore = 800;

  constructor (name) {
    this.name = name;
  }

  addStudentsFromFile(arr) {
    sortByRating(arr);
    arr.forEach (function(item, i, arr) {
      const student = new Student(item);
      const studentRating = getRating (student);
      if (studentRating >= University.minBudgetScore && i < University.maxBudgetStudents - 1) {
        student.isSelfPayment = false;
      }
      universityStudents.push(student);
    })
  }

  addNewStudent({name, surname, ratingPoint, schoolPoint}) {
    const student = new Student({name, surname, ratingPoint, schoolPoint});
    const studentRating = getRating (student);
    const lastBudgetStudentRating = getRating (universityStudents[University.maxBudgetStudents - 1]);
    if (studentRating >= University.minBudgetScore && studentRating > lastBudgetStudentRating) {
      student.isSelfPayment = false;
      universityStudents.push(student);
      universityStudents[University.maxBudgetStudents].isSelfPayment = true;
    } else {
      universityStudents.push(student);
    }
    sortByRating(universityStudents);
  }

  allStudents (arr) {
    sortById(arr);
    console.log(arr);
  }
  
  budgetStudents (arr) {
  const budgetStudents = [];
  for (let value of arr) {
    if (!value.isSelfPayment) {
      budgetStudents.push(value);
    }
  }
  console.log(budgetStudents);
  }
}

function getRating (item) {
  return rating = item.schoolPoint + item.ratingPoint;
}

function sortByRating(arr) {
  arr.sort((a, b) => (b.schoolPoint + b.ratingPoint) - (a.schoolPoint + a.ratingPoint));
}

function sortById(arr) {
  arr.sort((a, b) => a.id - b.id);
}

const university = new University ('ZNU');
university.addStudentsFromFile(students); 

const newStudent = {
  name : 'Yuliya',
  surname : 'Test',
  ratingPoint : 1200,
  schoolPoint :1200
}

university.addNewStudent(newStudent);
university.allStudents(universityStudents);
university.budgetStudents(universityStudents);




