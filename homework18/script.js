/*
Создайте класс Worker со следующими свойствами name, surname, rate, days. Напишите внутри класса метод getSalary(), который считает зарплату (рейт умноженный на количество отработанных дней) и метод getInfo(), который возвращает строку с информацией о полученой зарплате сотрудника

<name> <surname> got $<money>
*/

class WorkerClass {
  constructor(array) {
    this.name = array.name;
    this.surname = array.surname;
    this.rate = array.rate;
    this.days = array.days;
    this.salary = this.getSalary();
  }
  getSalary() {
    return this.salary = this.rate * this.days;
  }
  getInfo() {
    console.log(`${this.name} ${this.surname} got ${this.salary}`);
  }
};

/*
Создайте класс Boss, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker и плюс дополнительное свойство totalProfit. Напишите метод getSalary(), который считает зарплату сотрудника так же как метод getSalary() класса Worker + 10% от прибыли (totalProfit)

Чтобы посчитать переменную totalProfit - нужно найти сумму зарплат всех сотрудников на позиции worker. Можете найти эту переменную отдельно вне классов с помощью методов массива и передавать в класс Boss уже константой.
*/

class BossClass extends WorkerClass{
  constructor() {
    super(key);
    this.salary = this.getSalary();
  }
  getSalary() {
    return super.getSalary() + totalProfit / 10;
  }
}

let totalProfit = empoyees.filter(({position}) => position === 'worker')
  .reduce(function (accumulator, worker) {
    return accumulator + worker.rate * worker.days;
  }, 0);

console.log(totalProfit);

/*
Создайте класс Trainee, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker, но его метод geSalary() работает следующим образом. Во время испытательного срока (до 60 дней) сотрудник получает 70% своей зарплаты, а после испытательного срока так же как обычный сотудник.
*/

class TraineeClass extends WorkerClass{
  constructor() {
    super(key);
    this.salary = this.getSalary();
  }
  getSalary() {
    if (this.days < 60) {
      return super.getSalary() * 0.7;
    } else {
      return super.getSalary();
    }
  }
}
/*
Возьмите массив объектов и создайте для каждого объекта этого массива новый объект соответсвующего класса (какой класс выбрать укажет свойсвто position).
Продемонстрируйте метод getInfo() для каждого объекта в массиве.
Напишите задачу двумя способами (!): через классы и функцию-конструктор.
*/

const listOfWorkers = [];
const listOfBosses = [];
const listOfTrainees = [];
for (key of empoyees) {
  if (key.position === 'worker') {
    const worker = new WorkerClass(key);
    worker.getInfo();
    listOfWorkers.push(worker);
  } else if (key.position === 'boss') {
    const boss = new BossClass(key);
    boss.getInfo(); 
    listOfBosses.push(boss);
  } else {
    const trainee = new TraineeClass(key);
    trainee.getInfo();
    listOfTrainees.push(trainee);
  }
}

console.log(listOfWorkers);
console.log(listOfBosses);
console.log(listOfTrainees);
