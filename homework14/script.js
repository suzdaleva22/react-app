/*
1) Напишіть функцію, яка змінює фоновий колір тексту останнього параграфу в блоці <body>, а також функцію, яка змінює блоки footer и main місцями.
*/

function setBgColor() {
  const parent = document.getElementById('main');
  const lastParagraph = parent.lastElementChild;
  lastParagraph.style.backgroundColor = 'red';
};
setBgColor();

function changeFooterPlace() {
  const parent = document.getElementById('wrapper');
  const footer = parent.firstElementChild;
  parent.append(footer);
}
changeFooterPlace();

/*
2) Напишіть функцію, яка питає у користувача дозволу додати картинку (confirm()) і у разу згоди додає картинку на сторінку (посилання на картинку користувач повинен задавати самостійно).
*/

function addImage() {
  let permission = confirm('Do you want to add a picture?');
  if (permission) {
    const image = document.createElement('img');
    image.src = prompt('Type the URL', 'https://i.pinimg.com/736x/59/f4/7f/59f47f6bdd86728d3cf36d90fd212380.jpg');
    image.style.width = '30%';
    document.querySelector('h1').after(image);
  }
}
addImage();

/*
3) Для цього завдання вам знадобиться файл index.html, який закріплений внизу.

Запросіть у користувача число і додайте в середину форми теги <input> (перед кнопкою Register), кількість яких дорівнює цьому числу.

Вимоги до тегу <input>:

кожен інпут повинен мати клас input-item, value = `Input ${index}`;
останній інпут повинен мати додатковий клас margin-zero;
створіть власний клас, з властивістю background-color і додайте його всім непарним інпутам;
очістіть значення кожному третьому інпуту і задайте йому атрибут placeholder з будь-яким текстом.
*/

const numberOfInputs = +prompt("Type the number of inputs in the form");

function createForm() {
  for (let i = 1; i <= numberOfInputs; i++) {
    let newInput = document.createElement("input");
    newInput.setAttribute("class", "input-item");
    newInput.setAttribute("value", "Input " + i);
    document.querySelector(".button").before(newInput);
    if (i == numberOfInputs) {
      newInput.classList.add("margin-zero");
    }
    if (i % 2) {
      newInput.style.backgroundColor = "#f8ecff";
    }
  }
}

function setNewAttribute() {
  let allInputs = document.querySelectorAll(".input-item");
  for (let i = 2; i < allInputs.length; i += 3) {
    allInputs[i].removeAttribute("value");
    allInputs[i].setAttribute("placeholder", "my placeholder");
  }
}

createForm();
setNewAttribute();


/*
4) (опціонально) Напишіть функцію, яка приймає аргумент number, створює матрицю у вигляді таблиці наступного типу:

перша ячейка у першому рядку має значення «1», а кожна наступна ячейка має значення на 1 більше;

перша ячейка другого рядка має значення «2» и так далі...

Напишіть фукнцію, яка змінює колір ячейкам в таблиці, які розташовані на зворотній діагоналі матриці.
*/

let table = document.querySelector('#matrix');

function createMatrix (num) {
  for (let i = 0; i < num; i++) {
    let newTr = document.createElement('tr');
    table.append(newTr);
    for (let j = 1; j <= num; j++) {
        let newTd = document.createElement('td');
        newTd.innerText = `${j + i}`;
        newTr.append(newTd);
    }
  }
}

createMatrix(5);

function setColor() {
  let allTr = document.querySelectorAll('tr');
  console.log(allTr);
  for (let i = 0; i < allTr.length; i++) {
    allTr[allTr.length - i - 1].children[i].style.background = 'red';
  }
}
setColor() ;

