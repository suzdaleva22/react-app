export const LOAD_FILMS = '[FILMS] Load Films';
export const LIKE_FILM = '[FILM] Like Film';
export const SORT_FILMS = '[FILMS] Sort Films';
export const SEARCH_FILMS = '[FILMS] Search Films';
export const SELECT_FILM = '[FILM] Select Film';

export const loadFilms = (films) => ({
    type: LOAD_FILMS,
    payload:{films} 
})

export const likeFilm = (film) => ({
    type: LIKE_FILM,
    payload: {film}
})

export const sortFilms = (films) => ({
    type: SORT_FILMS,
    payload: {films} 
})

export const searchFilms = (films) => ({
    type: SEARCH_FILMS,
    payload: {films} 
})

export const selectFilm = (film) => ({
    type: SELECT_FILM,
    payload: {film}
})
