import React from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux'
import { store } from "./store/store"

import RoutesList from "./routes/RoutesList";

function App() {
  return (
    <>
  <Provider store={store}>
  <BrowserRouter>
        <RoutesList />
      </BrowserRouter>
  </Provider>


    </>
  );
}

export default App;
