import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from '@mui/material/FormControl';
import './Select.css';
import { forwardRef } from "react";

//forwardRef

export default function SexBasicSelect(props, ref) {
  const sex = ["male", "female"];
  const [selectedSex, setSelectedSex] = React.useState("");

  const handleChange = (event) => {
    setSelectedSex(event.target.value);
  };

  return (
    <>
    <FormControl fullWidth className="form-item">
      <InputLabel id="demo-simple-select-label">Sex</InputLabel>
      <Select 
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={selectedSex}
        label="Sex"
        onChange={handleChange}
        {...props}
        inputRef={ref}
      >
        <MenuItem value={sex[0]}>Male</MenuItem>
        <MenuItem value={sex[1]}>Female</MenuItem>
      </Select>
      </FormControl>
    </>
  );
}
