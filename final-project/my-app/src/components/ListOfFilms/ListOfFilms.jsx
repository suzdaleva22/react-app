import ActionAreaCard from "../../components/Card/Card";
import "./ListOfFilms.css";


export default function ListOfFilms({ films , likeFilm}) {

  return (
    <>
      <div className="content">
        <section className="results" id="media_results">
          <div className="media_items results">
            <div className="page-wrapper">
              {films.map((film) => (
                  <div className="card" key={film.id}>
                  <ActionAreaCard film={film} likeFilm={likeFilm}/>
                </div>
              ))}
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
