import { useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";

export default function NotFound() {
    const navigate = useNavigate();
    function navigateBack() {
        navigate(-1);
      }
    return (
        <>
        <h1>Not found any data</h1>
        <Button variant="contained" onClick={navigateBack}>Go Back</Button>
        </>
    )
}
