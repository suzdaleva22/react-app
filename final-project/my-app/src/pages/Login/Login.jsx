import * as React from "react";
import "./Login.css";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Input from "../../components/UI/Input/Input";
import { useNavigate } from "react-router-dom";

export default function Login(props) {
  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();
    localStorage.setItem("AUTH_TOKEN", "qwerty");
    navigate("/films");
  }
  function navigateRegistration() {
    navigate("/registration");
  }
  return (
    <>
      <React.StrictMode>
        <div
          className="box"
          sx={{
            backgroundColor: "white",
            p: "5px",
          }}
        >
          <Typography className="form-item" variant="h6" component="h6">
            Login
          </Typography>
          <form
            sx={{
              width: "100%",
            }}
          >
            <Input style={{ marginBottom: "20px" }} label="Username" />

            <Input style={{ marginBottom: "20px" }} label="Password" />

            <Button
              variant="contained"
              type="submit"
              onClick={handleSubmit}
              className="log"
            >
              Login
            </Button>
          </form>
        </div>
      </React.StrictMode>
      <div className="registration">
        <Typography className="form-item text" variant="subtitle1" gutterBottom>
          Don't have an account yet?
        </Typography>
        <Button variant="text" onClick={navigateRegistration} className="log">
          Registration
        </Button>
      </div>
    </>
  );
}
