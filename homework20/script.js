/*
1) Создайте Promise, которое должно выполнять следующие действия:

через 2 секунды он должен создать переменную number со случайным значением от 1 до 6;
если 1 <= number <= 5, вывести в консоли сообщение "Start the game..." и вернуть число;
если number = 6, вернуть ошибку;
Затем должна быть запущена функция-потребитель (than()), которая выполняет следующие

действия:

если number = 1, вывести сообщение "Stay here" ;
если number >= 2, вывести сообщение "Go <number> steps" .
Используйте catch(), чтобы отловить ошибку и выведите сообщение "Exit" .
*/
{
  const promise = new Promise((resolve, reject) => {
    const randomNumber = getRandomIntInclusive(1, 6);
    console.log(randomNumber);
    setTimeout(() => {
      if (randomNumber <= randomNumber && randomNumber <= 5) {
        console.log('Start the game...')
        resolve(randomNumber);
      } else {
        reject();
      }
    }, 2000);
  });

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  promise.then((randomNumber) => {
    if (randomNumber === 1) {
      console.log('Stay here');
    }
  }, );

  promise.then((randomNumber) => {
    if (randomNumber >= 2) {
      console.log(`Go ${randomNumber} steps`);
    }
  }, );

  promise.catch(() => {
    console.log(`Exit`);
  }, );
}

/*
2) Создайте функцию goToShop(), которая возвращает успешный Promise с количеством купленных продуктов (число взять из prompt()). Создайте функцию makeDinner(), которая возвращает Promise с таймером. Через 3 секунды промис должен завершиться с текстом ‘Bon Appetit’.

Если функция goToShop() возвращает меньше чем 4 продукта, то вернуть отклоненный промис с текстом ‘Too low products’, иначе вызвать функцию makeDinner() и вывести в консоль результат работы функции.

Если промис возвращается с ошибкой, то вывести ошибку в консоль в console.error();

*/
{
  const numberOfProducts = +prompt('Type number of Products');

  function goToShop() {
    const promise = new Promise(function (resolve, reject) {
      if (numberOfProducts > 3) {
        resolve(numberOfProducts);
      } else {
        reject();
        console.log(`Too low products`);
      };
    }, );

    promise.then(makeDinner);

    promise.catch(function () {
      console.log(`Error`);
    })
  }

  goToShop();

  function makeDinner() {
    const promise = new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 3000);
    });

    promise.then(() => {
      console.log(`Bon appetit`);
    });
  }
}