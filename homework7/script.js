/*
   Напишіть функцію parseDate(), яка показує поточну дату і час чітко в заданому форматі.

для дати (Thu Jan 06 2022 22:03:07) - new Date(2022, 0, 6, 22, 3, 7)
Today is: Thursday.
Current time is: 10 PM : 03 : 07

для дати (Sat May 05 2018 04:15:09) - new Date(2018, 4, 5, 4, 15, 9)
Today is: Saturday.
Current time is: 04 AM : 15 : 09

для дати (Sun Nov 30 2025 12:00:59) - new Date(2025, 11, 0, 12, 0, 0)
Today is: Sunday.
Current time is: 12 PM : 00 : 59
*/

const myDate = new Date();
let formatDayOfWeek = ''; 
  /* let formatDayOfWeek = ''; // вопросик: когда я НЕ объявляю переменную здесь (let formatDayOfWeek...), то ошибку не выдает и я могу ее использовать в global scope. Как здесь правильно делать? Пример использую переменную formatHours на 53 строке, не объявляя ее */

function parseDate(date) {
  getMyDay(date.getDay());
  getMyHours(date.getHours());
  getMyMinutes(date.getMinutes());
  getMySeconds(date.getSeconds());

  console.log(`Today is: ${formatDayOfWeek}.\nCurrent time is: ${formatHours} : ${formatMinutes} : ${formatSeconds}`);
  };

function getMyDay(dayOfWeek) {
  switch (dayOfWeek){
    case 0: formatDayOfWeek = 'Sunday';
    break;
    case 1: formatDayOfWeek = 'Monday';
    break;
    case 2: formatDayOfWeek = 'Tuesday';
    break;
    case 3: formatDayOfWeek = 'Wednesday';
    break;
    case 4: formatDayOfWeek = 'Thursday';
    break;
    case 5: formatDayOfWeek = 'Friday';
    break;
    case 6: formatDayOfWeek = 'Saturday';
    break;
  }; 
};

function getMyHours(hours) {
  if (hours > 12 && hours < 20) {
    formatHours = '0' + (hours - 12) + ' PM';
  } else if (hours > 20) {
    formatHours = (hours - 12) + ' PM';
  } else if (hours < 10){
    formatHours = '0' + hours + ' AM';
  } else if (hours === 12){
    formatHours = hours + ' PM';
  }
  else {
    formatHours = hours + ' AM';
  };
};

function getMyMinutes(minutes) {
 if (minutes < 10) {
  formatMinutes = '0' + minutes;
 } else {
  formatMinutes = minutes;
 };
};

 function getMySeconds(seconds) {
  if (seconds < 10 && seconds !== 0) {
    formatSeconds = '0' + seconds;
   } else if (seconds === 0) {
    formatSeconds = 59;
   } else {
    formatSeconds = seconds;
   };
 };

parseDate(myDate);

/*
Напишіть функцію getRandomInteger(min, max), яка повертає ціле число в заданому діапазоні чисел, які передані в аргументах функції.

Напишіть програму, яка питає у користувача ціле число і порівнює його зі створеним числом за допомогою функції getRandomInteger.

Якщо користувач ввів невірне число, виведіть в консоль повідомлення про помилку.

Якщо числа співпадають, вивести в консоль Good work, якщо ні - Not matched.
*/

function getRandomInteger(min, max) {
  randomInteger = Math.floor(Math.random() * (max - min + 1)) + min;
  return randomInteger;
};

let randomInteger;
let userNumber = +prompt('Enter your integer number');

getRandomInteger(3, 5);

if (isNaN(userNumber) || !Number.isInteger(userNumber) || userNumber <= 0) {
  console.error('Enter not a integer number');
} else if (userNumber === randomInteger) {
  console.log('Good work');
} else {
  console.log('Not matched');
};

/*
3) Напишіть функцію getDecimalNumber(arr), яка приймає в якості аргументу масив з чисел 0 або 1, і повертає число в десятичній системі еквівалентне заданому.

Наприклад, массив [0, 0, 0, 1] розглядається як 0001 і дорівнює 1.

Приклади для перевірки:

getDecimalNumber([0, 0, 0, 1]) // 1
getDecimalNumber([0, 0, 1, 0]) // 2
getDecimalNumber([1, 1, 1, 1]) // 15
getDecimalNumber([1, 1, 1, 0, 0, 1]) // 57
Массив може бути довільної довжини. В масиві не повинно бути ніяких других даних окрім чисел 0 і 1. В цій задачі забороняється використовувати number.toString() чи parseInt(), давайте спробуємо написати алгоритм самостійно :)
*/


const lengthRandomArr = getRandomInteger(4, 20);
let randomArr = [lengthRandomArr];

 for (let i = 0; i < lengthRandomArr; i++) {
  randomArr[i] = getRandomInteger(0, 1);
 };

let numberAfter = 0;

function convertArrayToDecNumber (array) {
  for (j = 0; j < array.length; j++) {
    numberAfter += array[j] * Math.pow(2, array.length - 1 - j);
  };
  console.log(numberAfter);
};

convertArrayToDecNumber (randomArr);

/*
Напишіть програму, яка питає у користувача число і ділить його на 2 стільки разів, поки воно не буде <= 50. Виведіть в консоль фінальне число і кількість операцій, які знадобились, щоб досягти цього числа.

Приклад виконання програми для number = 100500;

Initial number is: 100500;
Attempts: 11;
Final number is: 49.072265625;

*/
{
let userNumber = +prompt('Enter your integer number more than 50');

if (isNaN(userNumber) || !Number.isInteger(userNumber) || userNumber <= 50) {
  console.error('You entered not an integer number which is more than 50');
} else {
  divideNumber();
};

function divideNumber () {
  let finalNumber = userNumber;
  let i;
  for (i = 0; finalNumber > 50; i++) {
    finalNumber /= 2;
  }
  console.log (`Initial number is: ${userNumber};\nAttempts: ${i};\nFinal number is: ${finalNumber};`);  
};
}

/*
 Напишіть функцію getExponent(number, exponent), яка приймає два аргументи число і степінь і возводить це число в задану степінь (як Math.pow(), яле без Math.pow() i оператора **).

Вирішить задачу двома способами: через цикл і через рекурсію.

getExponent(2,3) // 8
getExponent(3,6) // 729
getExponent(0,0) // 1
*/

function getExponent(number, exponent) {
  let power = 1;
  for (let i = 1; i <= exponent; i++) {
    power *= number;
  }
  return power;
}
console.log(getExponent(2,4));


function getExponentRecurs(number,exponent) {
	if (exponent > 1) {
	  return number *= getExponentRecurs(number,exponent - 1);
	} else {
	  return number;
	}
}
console.log(getExponentRecurs(2,4));
