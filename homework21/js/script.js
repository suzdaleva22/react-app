const container = document.querySelector('.container');

function createCard(data) {
	const card = document.createElement('div');
	card.classList.add('card');

	const cardInfo = document.createElement('div');
	cardInfo.classList.add('card-info');

	const cardTitle = document.createElement('div');
	cardTitle.classList.add('title');
	const cardTitleH1 = document.createElement('h1');
	cardTitleH1.innerHTML = data[key].name;
	cardTitle.append(cardTitleH1);

	const cardStatus = document.createElement('div');
	cardStatus.classList.add('status');
	const cardLiveStatus = document.createElement('div');
	cardLiveStatus.classList.add('live-status');

	if (data[key].status === 'Dead') {
		cardLiveStatus.classList.add('dead');
	};

	if (data[key].status === 'unknown') {
		cardLiveStatus.classList.add('unknown');
	};

	const cardStatusP = document.createElement('p');
	const cardStatusPText = data[key].status;
	cardStatus.append(cardLiveStatus);
	cardStatusP.append(cardStatusPText);
	cardStatus.append(cardStatusP);
	cardTitle.append(cardStatus);
	cardInfo.append(cardTitle);

	const cardContent = document.createElement('div');
	cardContent.classList.add('content');
	const cardContentText = data[key].location.name;
	cardContent.append(cardContentText);
	cardInfo.append(cardContent);

	card.append(cardInfo);

	const cardImage = document.createElement('div');
	cardImage.classList.add('card-image');
	const image = document.createElement('img');
	image.src = data[key].image;
	image.alt = 'Some image';
	cardImage.append(image);
	card.append(cardImage);

	container.append(card);
}

/*
1) Напишите функцию, которая получает с сервера массив персонажей мультфильма и отрисовывает их на странице в карточках.

Данные в карточках заполнять согласно тестовому примеру в html файле.

Если персонаж мертв, нужно добавлять класс dead к диву с классом 'live-status'.

По-умолчанию, загружаем 10 случайных персонажей (создать функцию для получения массива 10ти случайных чисел, а потом передать их в URL );
*/

const randomNumbers = [];
for (let i = 0; i < 10; i++) {
	randomNumbers.push(getRandomIntInclusive(0, 826));
}

function getRandomIntInclusive(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

fetch(`https://rickandmortyapi.com/api/character/${randomNumbers}`)
	.then(response => {
		let characters = response.json();
		console.log(characters);
		return characters;
	})
	.then(addCharacters);

function addCharacters(characters) {
	for (key in characters) {
		createCard(characters);
	}
}

/*
2) Напишите функцию, которая по клику на фильтр вверху фильтрует персонажей по соответствующим критериям (male/female) или (alive/dead). Учтите, что фильтрация может быть как по одному признаку так и по обоим одновременно.

Ожидаемый результат приведен на скринах ниже:
10 персонажей с id от 1 до 10;
список персонажей отфильтрованных по критерию gender=female;
  */

function filter() {
	const sex = document.getElementsByName('gender');
	for (let key in sex) {
		sex[key].onchange = filterSex;
	}

	function filterSex() {
		clearBox('.container');
		if (this.value === 'male') {
			getFilteredCharacters('?gender=male');
		} else if (this.value === 'female') {
			getFilteredCharacters('?gender=female');
		}
	}

	const live = document.getElementsByName('status');
	for (let key in live) {
		live[key].onchange = filterLive;
	}

	function filterLive() {
		clearBox('.container');
		if (this.value === 'dead') {
			getFilteredCharacters('?status=dead');
		} else if (this.value === 'alive') {
			getFilteredCharacters('?status=alive');
		}
	}
}
filter();

function clearBox(element) {
	console.log(document.querySelector(element));
	document.querySelector(element).innerHTML = "";
}

function getFilteredCharacters(filter) {
	fetch(`https://rickandmortyapi.com/api/character/${filter}`)
		.then(response => {
			let filteredCharacters = response.json();
			return filteredCharacters;
		})
		.then(addfilteredCharacters);
}

function addfilteredCharacters(filteredCharacters) {
	for (key in filteredCharacters.results) {
		console.log(key);
		createCard(filteredCharacters.results);
	}
}