import * as React from "react";
import Button from "@mui/material/Button";
import FreeSolo from "../Search/Search";
import Typography from "@mui/material/Typography";
// import BasicSelect from "../UI/Select/Select";
import "./Filter.css";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useForm } from "react-hook-form";
import InputLabel from "@mui/material/InputLabel";
import { genresAPI } from "../../api/tmbdGenres";
import { useState, useEffect } from "react";

import MenuItem from "@mui/material/MenuItem";

export default function Filter() {
  const { register } = useForm({});

  const onSubmit = (data) => {
    console.log(data);
    //navigate('/films');
  };

  const [genre, setGenre] = React.useState("");
  const [genres, setGenres] = useState([]);

  async function getGenres() {
    try {
      const { data } = await genresAPI();
      setGenres(data);
    } catch (error) {
      console.log(error);
    }
  }

  const getAllGenres = (genres) => {
    let content = [];
    for (let key in genres) {
      const genre = genres[key];
      content.push(
        <MenuItem value={genres[key].id} key={genres[key].id}>
          {genres[key].name}
        </MenuItem>
      );
    }
    return content;
  };

  useEffect(() => {
    getGenres();
  }, []);

  const handleChange = (event) => {
    setGenre(event.target.value);
  };
  return (
    <>
      {/* <FreeSolo /> */}

      <FormControl fullWidth className="select">
        <InputLabel id="demo-simple-select-label">Genre</InputLabel>
        <Select
          {...register("genre")}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={genre}
          label="Genre"
          onChange={handleChange}
        >
          {getAllGenres(genres.genres)}
        </Select>
      </FormControl>

      <div>
        <Button variant="contained">Apply</Button>
      </div>
      <div>
        <Button variant="text">Clean Filter</Button>
      </div>
    </>
  );
}
