import "./Film.css";
import Typography from "@mui/material/Typography";
import { genresAPI } from "../../api/tmbdGenres";
import { useState, useEffect } from "react";

function Film({ film }) {
  const genres = { ...film.genres };

  const getGenres = (genres) => {
    let content = [];
    for (let key in genres) {
      const genre = genres[key];
      content.push(<li key={genre.id}>{genre.name}</li>);
    }
    return content;
  };

  return (
    <div className="film-wrapper">
      <div className="poster">
        <img
          src={"https://image.tmdb.org/t/p/w500" + film.poster_path}
          alt={film.original_title}
        />
      </div>
      <div className="description">
        <Typography variant="h3">{film.original_title}</Typography>
        <Typography variant="h7">
          {film.release_date} / ({film.original_language})
        </Typography>
        <div className="genres">
          <Typography variant="h6">Genres:</Typography>
          <ul>{getGenres(genres)}</ul>
        </div>

        <div className="overview">
          <Typography variant="h6">Overview</Typography>
          <Typography variant="h7">{film.overview}</Typography>
        </div>
      </div>
    </div>
  );
}

export default Film;
