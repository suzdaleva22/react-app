
import { getFilmsDB, getFilm } from "../api/tmbdDB";
import { loadFilmsSuccess, loadFilmsError, loadFilmError, loadFilmSuccess } from "../actions/posts";


export const fetchFilms = (params) => {
  return async (dispatch) => {
    try {
      const { data, headers } = await getFilmsDB(params);
      const totalCount = +headers['x-total-count'];
      dispatch(loadFilmsSuccess(data, totalCount));
    } catch (err) {
      dispatch(loadFilmsError(err));
    }
  };
};
export const fetchFilmById = (id) => {
    return async (dispatch) => {
      try {
        const { data } = await getFilm(id);
        dispatch(loadFilmSuccess(data));
      } catch (err) {
        dispatch(loadFilmError(err));
      }
    };
  };