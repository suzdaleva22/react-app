import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

import "./Card.css";
import CardActions from "@mui/material/CardActions";
import IconButton from "@mui/material/IconButton";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { useNavigate } from "react-router-dom";

export default function ActionAreaCard(film, { likeFilm }) {
  const navigate = useNavigate();

  function navigateToFilmInfo(filmId) {
    navigate(`/films/${filmId}`, { replace: true });
  }
  return (
    <Card sx={{ maxWidth: 345 }} style={{ margin: "10px" }} className="card">
      <CardActionArea onClick={() => navigateToFilmInfo(film.film.id)}>
        <CardMedia
          className="poster"
          component="img"
          image={"https://image.tmdb.org/t/p/w500" + film.film.poster_path}
          alt={film.film.name}
        />
        {/* <FloatingActionButtons  /> */}
        <CardContent>
          <Typography
            gutterBottom
            variant="h6"
            component="div"
            style={{ fontWeight: "500", lineHeight: "1em" }}
          >
            {film.film.original_title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {film.film.release_date}
          </Typography>
        </CardContent>
      </CardActionArea>

      <CardActions disableSpacing>
        <IconButton
          aria-label="add to favorites"
          onClick={() => likeFilm(film.film.id)}
        >
          <FavoriteIcon
            style={{
              color: `${film.film.isFavorite ? "red" : "gray"}`,
            }}
          />
        </IconButton>
      </CardActions>
    </Card>
  );
}
