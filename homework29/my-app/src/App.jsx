import { useState } from "react";
import "./App.css";
import AddSongForm from "./FormAddNewSong/FormAddNewSong";
import SongsList from "./SongList/SongsList";
import { songsData } from "./data/songsData";
import Dropdown from "./Sorting/Sorting";
import { sortParameters } from "./data/sortingData";

function App() {
  const [songsCounter, setCounter] = useState(songsData.length);
  const [songs, setSongsList] = useState(songsData);
  const [items, setItem] = useState(sortParameters);

  function sortSongs(selectedSortId) {
    if (selectedSortId == 0) {
      const newSortSongs = [...songs].sort((song) => (song.isLiked ? -1 : 1));
      setSongsList(newSortSongs);
    } else if (selectedSortId == 1) {
      const newSortSongs = [...songs].sort((song) => (song.isLiked ? 1 : -1));
      setSongsList(newSortSongs);
    } else {
      const newSortSongs = [...songs];
      setSongsList(newSortSongs);
    }
  }

  function increment() {
    setCounter(songsCounter + 1);
  }

  function decrement() {
    setCounter(songsCounter - 1);
  }

  function addNewSong(song) {
    setSongsList([song, ...songs]);
    increment();
  }
  function removeSong(id) {
    const newSongs = songs.filter((song) => song.id !== id);
    setSongsList(newSongs);
    decrement();

  }
  function likeSong(songId) {
    const updatedSongs = songs.map((song) => {
      if (song.id === songId) {
        return {
          ...song,
          isLiked: !song.isLiked,
        };
      }
      return song;
    });
    setSongsList(updatedSongs);
  }
  return (
    <>
      <h2>Playlist on REACT</h2>
      <AddSongForm addNewSong={addNewSong} />
      <Dropdown 
        items={items} 
        sortSongs={sortSongs} />
      <SongsList
        songs={songs}
        likeSong={likeSong}
        removeSong={removeSong}
      />
      <p className="count-title">
        Count of songs: <span className="count"> {songsCounter} </span>
      </p>
    </>
  );
}

export default App;
