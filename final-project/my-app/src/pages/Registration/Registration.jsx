import * as React from "react";
import "./Registration.css";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Input from "../../components/UI/Input/Input";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import { useNavigate } from "react-router-dom";

const schema = yup
  .object({
    firstName: yup
      .string()
      .matches(/^[a-zA-Z]+$/i, "This field must contain only letters")
      .required("This field is required"),
    lastName: yup
      .string()
      .matches(/^[a-zA-Z]+$/i, "This field must contain only letters")
      .required("This field is required"),
    username: yup
      .string()
      .matches(
        /^[a-z][a-zA-Z0-9_*.]/,
        "This field must contain only small letters, numbers, symbols(_*.)"
      )
      .required("This field is required"),

    email: yup
      .string()
      .email("Email is incorrect")
      .required("This field is required"),
    password: yup.string().min(8).max(32).required("This field is required"),
  })
  .required();

export default function Registration(props) {
  const navigate = useNavigate();

  function navigateLogin() {
    navigate("/login");
  }
  const {
    register,
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const onSubmit = (data) => {
    console.log(data);
    localStorage.setItem("AUTH_TOKEN", "qwerty");
    navigate("/films");
  };

  const [sex, setSex] = React.useState("");

  const handleChange = (event) => {
    setSex(event.target.value);
  };
  return (
    <>
      <React.StrictMode>
        <div
          className="box"
          sx={{
            backgroundColor: "white",
            p: "5px",
          }}
        >
          <Typography className="form-item" variant="h6" component="h6">
            Register
          </Typography>
          <form
            onSubmit={handleSubmit(onSubmit)}
            sx={{
              width: "100%",
            }}
          >
            <Input {...register("firstName")} label="First name" />
            <p className="error">{errors.firstName?.message}</p>
            <Input {...register("lastName")} label="Last name" />
            <p className="error">{errors.lastName?.message}</p>
            <Input {...register("username")} label="Username" />
            <p className="error">{errors.username?.message}</p>

            <FormControl fullWidth className="select">
              <InputLabel id="demo-simple-select-label">Sex</InputLabel>
              <Select
                {...register("sex", { required: true })}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={sex}
                label="Sex"
                onChange={handleChange}
              >
                <MenuItem value="male">Male</MenuItem>
                <MenuItem value="female">Female</MenuItem>
              </Select>
            </FormControl>
            {/* <LocalizationProvider dateAdapter={AdapterDayjs}>
                                                  <DatePicker
                                                    label="Basic example"
                                                    value={value}
                                                    onChange={(newValue) => {
                                                      setValue(newValue);
                                                    }}
                                                    renderInput={(params) => <TextField {...params} />}
                                                  />
                                                </LocalizationProvider> */}

            <Input {...register("email")} label="Email" />
            <p className="error">{errors.email?.message}</p>
            <Input {...register("password")} label="Password" />
            <p className="error">{errors.password?.message}</p>
            <Input {...register("confirmPassword")} label="Confirm Password" />
            <Button variant="contained" type="submit" className="reg">
              Registration
            </Button>
          </form>
        </div>
      </React.StrictMode>
      <div className="login">
        <Typography
          className="form-item text"
          variant="overline"
          display="block"
          gutterBottom
        >
          Do you already have an account?
        </Typography>
        <div>
          <Button variant="text" onClick={navigateLogin} className="log">
            Login
          </Button>
        </div>
      </div>
    </>
  );
}
