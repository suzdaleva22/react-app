import axios from "axios";
import { ACCESS_KEY } from "../config/config";




  const tmbdAPI = axios.create( {
    baseURL: 'https://api.themoviedb.org',
    headers: {
      Authorization: `Bearer ${ACCESS_KEY}`,
    },
  })

export const getFilmsDB = (page) => {
    return tmbdAPI.get('/3/movie/popular', {
        params: {
          api_key: '75b89506372d6567413f8aa218c3fd5b',
            page: page,
        }
    });
}

export const getSearchedFilms = (query) => {
  return tmbdAPI.get('/3/search/movie', {
      params: {
        api_key: '75b89506372d6567413f8aa218c3fd5b',
          query: query,
      }
  });
}

export const getFilm = (id) => {
  return tmbdAPI.get(`/3/movie/${id}`, {
      params: {
        api_key: '75b89506372d6567413f8aa218c3fd5b',
      }
  });
}