import Filter from "../../components/Filter/Filter";
import ListOfFilms from "../../components/ListOfFilms/ListOfFilms";
import { useState, useEffect, useMemo } from "react";
import axios from "axios";
import Header from "../../components/Header/Header";

import SearchMovie from "../../components/Search/Search";
import { getFilmsDB, getSearchedFilms } from "../../api/tmbdDB";
import Typography from "@mui/material/Typography";
import Checkbox from "@mui/material/Checkbox";
import "./Films.css";
import { useSelector, useDispatch } from "react-redux";
import { loadFilms, sortFilms } from "../../actions/films";

function Films() {
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  const randomPage = getRandomIntInclusive(1, 500);
  const [checked, setChecked] = useState(false);

  const { films } = useSelector((state) => state.films);
  const dispatch = useDispatch();

  useEffect(() => {
    getFilmsDB(randomPage).then((loadedFilms) =>
      dispatch(loadFilms(loadedFilms.data.results))
    );
  }, []);

  function onSearchSubmit(query) {
    getSearchedFilms(query).then((loadedFilms) =>
      dispatch(loadFilms(loadedFilms.data.results))
    );
  }

  // function filterFilms() {
  //   const updatedFilms = films.sort((film) => {
  //     return film.isFavorite ? -1 : 1;
  //   });
  //   return updatedFilms;
  // }

  // function sortFilms() {
  //   dispatch(sortFilms(sortedFilms.data.results))
  // }
  // const handleChange = (event) => {
  //   setChecked(event.target.checked);
  //   sortFilms();
  // };

  // function likeFilm(filmId) {
  //   const updatedFilms = films.map((film) => {
  //     if (film.id === filmId) {
  //       return {
  //         ...film,
  //         isFavorite: !film.isFavorite,
  //       };
  //     }
  //     return film;
  //   });
  //   setFilms(updatedFilms);
  // }

  return (
    <div className="app-wrapper">
      <Header />
      <div className="checkbox">
        <Checkbox
          checked={checked}
          // onChange={handleChange}
          inputProps={{ "aria-label": "controlled" }}
          label="First favorite films"
        />
        <label htmlFor="">First favorite films</label>
      </div>
      <div className="sidebar">
        <Typography variant="h6" component="h6">
          Search and Filter
        </Typography>
        <div className="search">
          <SearchMovie onSearchSubmit={onSearchSubmit} />
        </div>

        <div>
          <Filter />
        </div>
      </div>

      {!films.length && (
        <div>
          <Typography className="form-item" variant="h6" component="h6">
            No results
          </Typography>
        </div>
      )}

      <ListOfFilms films={films} />
    </div>
  );
}

export default Films;
