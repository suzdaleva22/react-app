import TextField from "@mui/material/TextField";
import { forwardRef } from "react";
import "./Input.css"


const Input = forwardRef((props, ref) =>
<TextField 
    className="form-item" 
    id="outlined-basic" 
    label="Last Name" 
    variant="outlined" 
    {...props} 
    inputRef={ref}/>
)
 export default Input;