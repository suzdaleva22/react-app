
import { lazy, Suspense } from "react";
import { Navigate } from "react-router-dom";

//const WelcomePage = lazy(() => import("../pages/WelcomePage/WelcomePage"));
const Login = lazy(() => import("../pages/Login/Login"));
const Registration = lazy(() => import("../pages/Registration/Registration"));
const Films = lazy(() => import("../pages/Films/Films"));
const FilmInfo = lazy(() => import("../pages/FilmInfo/FilmInfo"));
const NotFound = lazy(() => import("../pages/NotFound/NotFound"));

function getComponent(Component) {
  const TOKEN = localStorage.getItem("AUTH_TOKEN");
  return TOKEN ? (
    <Suspense>
      <Component />
    </Suspense>
  ) : (
    <Navigate to="/login" />
  );
}

export const routes = [
  {
    path: "/films",
    element: getComponent(Films),
    // children: [
    //     {
    //         path: '/:id',
    //         element: <FilmInfo />
    //     }
    // ]
  },
  {
    path: "/registration",
    element: <Registration />,
  },
  // {
  //     path: '/',
  //     element: <WelcomePage />
  // },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/films/:id",
    element: getComponent(FilmInfo),
  },
  {
    path: "*",
    element: <NotFound />,
  },
];
