
import { Fragment } from "react";
import "./App.css";
import Songs from "./songs";
import Counter from "./counter";

function App() {
  return (
    <>
      <div className="songs-wrapper">
        <ul className="songs"></ul>
        <Songs />
      </div>
      <Counter />
    </>
  );
}

export default App;
