import { useState } from "react";
import { useEffect } from "react";
import Film from "../../components/Film/Film";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Header from "../../components/Header/Header";
import { getFilm } from "../../api/tmbdDB";
import { selectFilm } from "../../actions/films";
import { useSelector, useDispatch } from "react-redux";
import Button from "@mui/material/Button";
import "./FilmInfo.css";

function FilmInfo() {
  const { id } = useParams();
  const navigate = useNavigate();
  const { selectedFilm: film } = useSelector((state) => state.films);
  const dispatch = useDispatch();

  useEffect(() => {
    getFilm(id).then((film) => dispatch(selectFilm(film.data)));
  }, [dispatch]);

  function navigateBack() {
    navigate("/films");
  }

  return (
    <div>
      <Header />

      <Button variant="contained" onClick={navigateBack} id="btn">
        Back
      </Button>
      {/* 
      {!film.length &&
 <h1>Not found any data</h1>

      } */}
      <Film film={film} className="filmContent" />
    </div>
  );
}

export default FilmInfo;
