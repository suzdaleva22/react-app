import { LOAD_FILMS, LIKE_FILM, SORT_FILMS, SEARCH_FILMS, SELECT_FILM } from "../actions/films";

export const initialState = {
    films: [],
    selectedFilm: {}
}

export const filmsReducer = function(state=initialState, action) {
    switch (action.type) {
        case LOAD_FILMS: 
            return {
                ...state,
                films: [...action.payload.films],
            }
            case SELECT_FILM: 
            return {
                ...state,
                selectedFilm: {...action.payload.film},
                // selectedFilm: state.films.find[film => film.id === action.payload.filmId],
            }
        default: 
        return state;
    }
    
    
}