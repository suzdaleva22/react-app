/*
  1) Напишіть програму, яка продемонструє роботу з масивом. Створіть масив із восьми елементів:

  '455' 87.15 true undefined null 'false' [] {}
  Виведіть інформацію о типі даних кожного елемента в консолі. Додайте значення 7 до кожного елементу масива і виведіть отримані значення в консолі.
*/
{
  const arr = ['455', 87.15, true, undefined, null, 'false', [], {}];

  for (let item in arr) {
    console.log(arr[item] + ':' + typeof(arr[item]));
    console.log(arr[item] + 7 + ':' + typeof((arr[item])+7));
  }
}
/*
Напишіть програму, яка питає у користувача число і створює масив numbers з випадкових цілих чисел в діапазоні від 0 до 10, довжина якого дорівнює числу, яке ввів користувач.

Виведіть створений масив numbers в консолі.

Скопіюйте массив numbers в новий масив. Кожен третій елемент нового масиву помножте на 3.

Виведіть новостворений масив в консолі.
*/

{
  const lengthArr = +prompt('Enter your number');
  let numbers = [];

  //  функцию взяла с developer.mozilla.org
  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; 
  };

  if (isNaN(lengthArr)) {
    console.error('You entered not a number');
  } else {
    for (i = 0; i < lengthArr; i++) {
      numbers[i] = getRandomIntInclusive(0, 10); 
    };
  };
  console.log(numbers);

  let numbersNew = [...numbers];

  for (j = 2; j < lengthArr; j += 3) {
    numbersNew[j] *= 3;
  };
  console.log(numbersNew);
}

/*

а) Створіть массив, який складається з повних імен всіх співробітників.

const fullNames =  your code ; // ['Karan Duffy', 'Brax Dalton', 'Jody Lam', ...]

*/

  const fullNames  = [];
   
  for (let user in empolyee) {
    fullNames[user] = `${empolyee[user]['name']} ${empolyee[user]['surname']}`;
  };
  console.log(fullNames);
 
/*
b) Знайдіть середнє значення всіх зарплат співробітників.

const average =  some number ;
*/

let salarySum = 0;  
   
for (let user in empolyee) {
  salarySum += empolyee[user]['salary'];
};
const average = Math.round(salarySum / (empolyee.length));
console.log(average);

/*
с) Виведіть в консоль імʼя чоловіка-пільговика (ключ isPrivileges=true) з самою великою зарплатою.

const maxPrivilegesMan = firstName lastName ;

*/

let highestSalary = 0;
let maxPrivilegesMan;

for (let user in empolyee) {
  if (empolyee[user]['isPrivileges'] && 
      empolyee[user]['gender'] === 'male' && 
      empolyee[user]['salary'] > highestSalary) {
        highestSalary = empolyee[user]['salary'];
        maxPrivilegesMan = `${empolyee[user]['name']}`;
  };  
};
console.log(maxPrivilegesMan);

/*
d) Виведіть в консоль повні імена (імʼя + прізвище) двох жінок з самим маленьким досвідом роботи (ключ workExperience).
*/

const sortedEmployee  = [...empolyee];
   
for (let n = 0; n <= 1; n++){
  for (let i = sortedEmployee.length - 1; i > 0; i--) {
    if (sortedEmployee[i]['gender'] === 'female'&& sortedEmployee[i]['workExperience'] < sortedEmployee[i - 1]['workExperience'] ) {
      const buffer = sortedEmployee[i];
      sortedEmployee[i] = sortedEmployee[i - 1];
      sortedEmployee[i - 1] = buffer;
    };
  };
  console.log(`${sortedEmployee[n]['name']} ${sortedEmployee[n]['surname']}`); 
};

/*
e) Виведіть в консоль інформацію, скільки всього заробили співробітники за весь час роботи в одній строці. Формат відповіді: <імʼя прізвище> - <сума>.

const result = your code ; // Karan Duffy - 10100
Brax Dalton - 14400
Jody Lam - 1440
*/

let result = '';

for (let user in empolyee) {
  let totalUserEarnings = empolyee[user]['salary'] * empolyee[user]['workExperience'];
  result += `${empolyee[user]['name']} ${empolyee[user]['surname']} - ${totalUserEarnings}.\n`;
};
console.log(result);