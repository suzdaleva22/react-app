import axios from "axios";
import { ACCESS_KEY } from "../config/config";


const tmbdAPI = axios.create( {
  baseURL: 'https://api.themoviedb.org',
  headers: {
    Authorization: `Bearer ${ACCESS_KEY}`,
  },
})

export const genresAPI = () => {
    return tmbdAPI.get('/3/genre/movie/list', {
        params: {
          api_key: '75b89506372d6567413f8aa218c3fd5b',
        }
    });
}
